const readline = require("readline");
let rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function calcTubeVol(radius, height) {
  const pi = 3.14;
  const vol = pi * (radius * radius) * height;
  //   returns only 2 decimal places
  return parseFloat(vol.toFixed(2));
}
function calcCubeVol(length) {
  return length ** 3;
}

function printQuestions() {
  const getCube = () => {
    rl.question("Please input cube length: ", (length) => {
      if (!isNaN(length)) {
        console.log("Cube volume is ", calcCubeVol(length));
        // call getTube after
        getTube();
      } else {
        console.log("Please input a number");
        getCube();
      }
    });
  };
  const getTube = () => {
    rl.question("Please input tube radius ", (radius) => {
      rl.question("Please input tube height ", (height) => {
        if (!isNaN(radius) && !isNaN(height)) {
          console.log("Tube volume is ", calcTubeVol(radius, height));
          // close rl after
          rl.close();
        } else {
          console.log("Please input a number");
          getTube();
        }
      });
    });
  };
  getCube();
}

printQuestions();

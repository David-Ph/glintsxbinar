const readline = require("readline");
let rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function calcTubeVol(radius, height) {
  const pi = 3.14;
  const vol = pi * (radius * radius) * height;
  //   returns only 2 decimal places
  return parseFloat(vol.toFixed(2));
}
function calcCubeVol(length) {
  return length ** 3;
}

function getTubeInput() {
  const getInput = new Promise((resolve, reject) => {
    rl.question("Input tube radius: ", (radius) => {
      rl.question("input tube height: ", (height) => {
        if (!isNaN(radius) && !isNaN(height)) {
          console.log("Tube volume is", calcTubeVol(radius, height));
          resolve("Thank you!");
        } else {
          console.log("Please input a number");
          (async function () {
            await getTubeInput();
          })().then(() => {
            resolve("Thank you!");
          });
        }
      });
    });
  });
  return getInput;
}

function getCubeInput() {
  const getInput = new Promise((resolve, reject) => {
    rl.question("Input cube length: ", (length) => {
      if (!isNaN(length)) {
        console.log("Cube volume is", calcCubeVol(length));
        resolve("Thank you!");
      } else {
        console.log("Please input a number");
        (async function () {
          await getCubeInput();
        })().then(() => {
          resolve("Thank you!");
        });
      }
    });
  });
  return getInput;
}

async function printQuestions() {
  await getTubeInput().then((message) => console.log(`${message}\n========`));
  await getCubeInput().then((message) => console.log(`${message}\n========`));
  rl.close();
}

printQuestions();
rl.on("close", () => {
  process.exit();
});

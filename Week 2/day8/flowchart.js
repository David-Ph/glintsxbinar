const documentsArray = ["ID", "Medical Certificate"];
let drivingSkill = 30;
let money = 5000;

function prepareDocuments() {
  console.log("Prepare ID");
  console.log("Prepare medical certificate");
}

function getDocuments() {
  documentsArray.push("ID");
  documentsArray.push("Medical Certificate");
  console.log("Documents secured");
}

function headToPoliceStation() {
  console.log("Head to police station");
}

function checkForNecessaryDocument(documents) {
  if (documents.includes("ID") && documents.includes("Medical Certificate")) {
    return true;
  }
  console.log("Please prepare ID and Medical Certificate");
  return false;
}

function registerForTest() {
  console.log("Register for test");
}

function doTest(skill) {
  if (skill > 50) {
    console.log("Pass the test");
    return true;
  } else {
    console.log("Failed. Please train and register again");
    return false;
  }
}

function trainForTest() {
  drivingSkill += 10;
}

function getLicense() {
  console.log("License secured!");
}

function completelyLegalWay(pay) {
  if (pay >= 350000) {
    console.log("Thank you for your service!");
    return true;
  } else {
    console.log("Not enough money!");
    return false;
  }
}

function main() {
  prepareDocuments();
  // if have enough money
  // go directly to the end
  let alternativeWay = completelyLegalWay(money);
  if (!alternativeWay) {
    headToPoliceStation();
    const necessaryDocument = checkForNecessaryDocument(documentsArray);
    // if no necessary document
    // go get the documents
    if (!necessaryDocument) {
      getDocuments();
    }
    registerForTest();
    // if failed to pass test
    // train until pass test
    let passTest = doTest(drivingSkill);
    while (!passTest) {
      trainForTest();
      passTest = doTest(drivingSkill);
    }
  }

  getLicense();
}

main();

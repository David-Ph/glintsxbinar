const index = require("../index");

function volumeprisma(Lengthtriangle, heighttriangle, heightprism) {
  return 0.5 * (Lengthtriangle * heighttriangle) * heightprism;
}
function input() {
  index.rl.question("Lengthtriangle: ", (Lengthtriangle) => {
    index.rl.question("heighttriangle: ", (heighttriangle) => {
      index.rl.question("heightprism: ", (heightprism) => {
        if (
          !index.isNotValidInput(Lengthtriangle) &&
          !index.isNotValidInput(heighttriangle) &&
          !index.isNotValidInput(heightprism)
        ) {
          console.log(
            `\nvolumeprisma: ${volumeprisma(
              Lengthtriangle,
              heighttriangle,
              heightprism
            )} \n`
          );
          index.main();
        } else {
          console.log(`Length, Width and Height must be a number\n`);
          input();
        }
      });
    });
  });
}

module.exports = { input };

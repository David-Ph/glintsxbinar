// Import readline
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// import function
const tube = require("./function/david_tube");
const cone = require("./function/kim_cone");
const sphere = require("./function/rezki_sphare");
const prism = require("./function/ruhul_volumeprismategaksegitiga");

function isNotValidInput(str) {
  return isNaN(str) || str <= 0 || str === null || str.match(/^ *$/) !== null;
}

function main() {
  console.log("Main Menu");
  console.log("===============================");
  console.log("What do you want to calculate? ");
  console.log("===============================");
  console.log("1. Tube");
  console.log("2. Sphere");
  console.log("3. Cone");
  console.log("4. Prism");
  console.log("5. Exit");
  rl.question("Choose option: ", (option) => {
    if (!isNotValidInput(option)) {
      //   if input is valid
      if (option == 1) {
        tube.input(); // run function for tube
      } else if (option == 2) {
        sphere.inputSphare();
      } else if (option == 3) {
        cone.input(); // run function for cone
      } else if (option == 4) {
        prism.input();
      } else if (option == 5) {
        rl.close(); // close program
      } else {
        console.log("Option must be 1 to 5! \n");
        main();
      }
    } else {
      // if option is not valid
      console.log("Please input valid option!");
      main();
    }
  });
}

main();

module.exports.rl = rl; // export rl to make another can run the readline
module.exports.isNotValidInput = isNotValidInput;
module.exports.main = main;

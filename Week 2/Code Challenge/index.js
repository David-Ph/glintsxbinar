const data = require("./lib/arrayFactory.js");
const test = require("./lib/test.js");

/*
 * Code Here!
 * */

// Optional
function clean(data) {
  return data.filter((i) => typeof i === "number");
}

// Should return array
function sortAscending(data) {
  // Code Here
  data = clean(data);

  for (let i = 0; i < data.length; i++) {
    // make a variable in each loop
    // to keep track if a sort happened
    let isSorted = 0;
    // loop from 0 to length - i
    // so we don't loop to the element
    // that has been sorted and put through the end
    for (let j = 0; j < data.length - i; j++) {
      if (data[j] > data[j + 1]) {
        // if sort happens, change value of isSorted to 1
        isSorted = 1;
        // swap element
        [data[j], data[j + 1]] = [data[j + 1], data[j]];
      }
      // keep looping until it reached the end of array
    }
    // if isSorted is 0, it means sorting has finished and we can just end it
    if (isSorted === 0) {
      break;
    }
  }
  return data;
}

// Should return array
function sortDecending(data) {
  // Code Here
  data = clean(data);
  // basically the same as above
  // but puts the smallest number to the last index
  for (let i = 0; i < data.length; i++) {
    let isSorted = 0;

    for (let j = 0; j < data.length - i; j++) {
      if (data[j] < data[j + 1]) {
        isSorted = 1;
        [data[j], data[j + 1]] = [data[j + 1], data[j]];
      }
    }
    if (isSorted === 0) {
      break;
    }
  }
  return data;
}
// DON'T CHANGE
test(sortAscending, sortDecending, data);

const people = [
  {
    name: "John",
    status: "Positive",
  },
  {
    name: "Mike",
    status: "Suspect",
  },
  {
    name: "Alice",
    status: "Positive",
  },
  {
    name: "Bob",
    status: "Suspect",
  },
  {
    name: "Charlie",
    status: "Negative",
  },
];

const getResult = () => {
  people.forEach((person) => {
    switch (person.status) {
      case "Negative":
        console.log(`${person.name} test result is ${person.status}`);
        break;
      case "Positive":
        console.log(`${person.name} test result is ${person.status}`);
        break;
      case "Suspect":
        console.log(`${person.name} is a ${person.status}`);
        break;
      default:
        console.log(`${person.name} is not tested`);
        break;
    }
  });
};

module.exports = {
  getResult,
};

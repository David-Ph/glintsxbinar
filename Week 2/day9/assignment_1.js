const array = ["tomato", "broccoli", "kale", "cabbage", "apple"];

array.forEach((veggies) => {
  if (veggies !== "apple") {
    console.log(`${veggies} is a healthy food, it's definitely worth to eat`);
  }
});

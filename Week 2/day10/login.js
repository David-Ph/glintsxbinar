const EventEmitter = require("events");
const readline = require("readline");
const { getResult } = require("../day9/assignment_2");
// initialize an instance because it's a class
const my = new EventEmitter();
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// registering a listener
my.on("Login Failed", function (email) {
  // TODO: saving the login trial count in the database
  console.log(email, "failed to login");
});
my.on("Login Success", function (email) {
  console.log(email, "successfully login");
});

const user = {
  login(email, password) {
    const passwordStoredInDatabase = "123456";

    if (password !== passwordStoredInDatabase) {
      my.emit("Login Failed", email); // Pass the email to the listener
      rl.close();
    } else {
      // Do something
      my.emit("Login Success", email);
      getResult();
      rl.close();
    }
  },
};
rl.question("Email: ", function (email) {
  rl.question("Password: ", function (password) {
    user.login(email, password); // Run login function
  });
});

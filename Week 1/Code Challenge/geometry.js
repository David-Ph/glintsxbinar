function calcCubeVol(length) {
  // check if length is a number
  if (typeof length !== "number") {
    return "Please input a number!";
  }

  return length ** 3;
}

function calcTubeVol(radius, height) {
  if (typeof radius !== "number" || typeof height !== "number") {
    return "Please input a number!";
  }

  const pi = 3.14;
  const vol = pi * (radius * radius) * height;
  //   returns only 2 decimal places
  return parseFloat(vol.toFixed(2));
}

console.log(calcCubeVol("hehe")); // "Please input a number!"
console.log(calcCubeVol(5)); // 125

console.log(calcTubeVol(10, 5)); // 1570
console.log(calcTubeVol(3.5, 2.2)); // 84.62

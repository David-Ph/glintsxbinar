let cars = require("../models/cars.json");

class CarController {
  getCars(req, res) {
    try {
      res.status(200).json({
        data: cars,
      });
    } catch (error) {
      res.status(500).json({
        message: error.error,
      });
    }
  }

  getOneCar(req, res) {
    try {
      const car = cars.filter((car) => {
        return car.id === eval(req.params.id);
      });

      res.status(200).json({
        data: car,
      });
    } catch (error) {
      res.status(500).json({
        message: error.error,
      });
    }
  }

  createCar(req, res) {
    try {
      cars.push(req.body);

      res.status(201).json({
        data: cars,
      });
    } catch (error) {
      res.status(500).json({
        message: error.error,
      });
    }
  }

  updateCar(req, res) {
    try {
      let car = cars.filter((car) => {
        return car.id === eval(req.params.id);
      });
      car[0].brand = req.body.brand;
      car[0].color = req.body.color;

      res.status(201).json({
        data: cars,
      });
    } catch (error) {
      res.status(500).json({
        message: error.error,
      });
    }
  }

  deleteCar(req, res) {
    try {
      cars = cars.filter((car) => {
        return car.id !== eval(req.params.id);
      });

      res.status(201).json({
        data: cars,
      });
    } catch (error) {
      res.status(500).json({
        message: error.error,
      });
    }
  }
}

module.exports = new CarController();

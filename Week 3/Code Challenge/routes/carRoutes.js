const express = require("express");
const router = express.Router();
const CarController = require("../controllers/carController");

// ? get all cars
router.get("/", CarController.getCars);

// ? get one specific car
router.get("/:id", CarController.getOneCar);

// ? create new car
router.post("/", CarController.createCar);

// ? update a car
router.put("/:id", CarController.updateCar);

// ? delete a car
router.delete("/:id", CarController.deleteCar);

module.exports = router;

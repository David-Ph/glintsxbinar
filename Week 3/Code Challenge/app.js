// ***************** //
// * APP DEPENDENCY *//
// ***************** //

const express = require("express");
const app = express();

// ***************** //
// * IMPORT ROUTES *//
// ***************** //

const carsRouter = require("./routes/carRoutes");

// ********** //
// * APP USE *//
// ********** //
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// ************* //
// * APP ROUTERS *//
// ************* //

app.use("/cars", carsRouter);

// *********** //
// * APP PORT *//
// *********** //

app.listen(3000, () => {
  console.log("Listening to 3000 ....");
});

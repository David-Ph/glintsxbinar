const axios = require("axios");
const fetch = require("node-fetch");

// ************* //
// *   FETCH   * //
// ************* //

// get /posts
fetch("https://jsonplaceholder.typicode.com/posts")
  .then((res) => res.json())
  .then((data) => console.log(data));

// get /posts/1
fetch("https://jsonplaceholder.typicode.com/posts/1")
  .then((res) => res.json())
  .then((data) => console.log(data));

// get /posts/1/comments
fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
  .then((res) => res.json())
  .then((data) => console.log(data));

// get /comments?postId=1
fetch("https://jsonplaceholder.typicode.com/comments?postId=1")
  .then((res) => res.json())
  .then((data) => console.log(data));

// ************* //
// *   Axios   * //
// ************* //

// get /posts
axios.get("https://jsonplaceholder.typicode.com/posts").then((resp) => {
  console.log(resp.data);
});

// get /posts/1
axios.get("https://jsonplaceholder.typicode.com/posts/1").then((resp) => {
  console.log(resp.data);
});

// get /posts/1/comments
axios
  .get("https://jsonplaceholder.typicode.com/posts/1/comments")
  .then((resp) => {
    console.log(resp.data);
  });

// get /comments?postId=1
axios
  .get("https://jsonplaceholder.typicode.com/comments?postId=1")
  .then((resp) => {
    console.log(resp.data);
  });

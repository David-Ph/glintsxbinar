const axios = require("axios");
const fetch = require("node-fetch");

// use this function to log data one by one
async function logData(getData) {
  const data = await getData();
  console.log(data);
}

// or use this to read all data at once
// using promiseAll
const readAll = async () => {
  let allData = await Promise.all([
    getPosts(),
    getPostOne(),
    getCommentsOnPostOne(),
    getCommentsOnPostIdOne(),
    getAxiosPosts(),
    getAxiosPostOne(),
    getAxiosCommentsOnPostOne(),
    getAxiosCommentsOnPostIdOne(),
  ]);

  allData.forEach((data) => {
    console.log(data);
    console.log("==========================\n");
  });
};

// uncomment this to call function readAll
// readAll();

// ************* //
// *   FETCH   * //
// ************* //

// get /posts
async function getPosts() {
  return await fetch("https://jsonplaceholder.typicode.com/posts")
    .then((resp) => resp.json())
    .then((data) => data);
}

// get /posts/1
async function getPostOne() {
  return await fetch("https://jsonplaceholder.typicode.com/posts/1")
    .then((resp) => resp.json())
    .then((data) => data);
}

// get /posts/1/comments
async function getCommentsOnPostOne() {
  return await fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
    .then((resp) => resp.json())
    .then((data) => data);
}

// get /comments?postId=1
async function getCommentsOnPostIdOne() {
  return await fetch("https://jsonplaceholder.typicode.com/comments?postId=1")
    .then((resp) => resp.json())
    .then((data) => data);
}

logData(getPosts);
logData(getPostOne);
logData(getCommentsOnPostOne);
logData(getCommentsOnPostIdOne);

// ************* //
// *   AXIOS   * //
// ************* //

async function getAxiosPosts() {
  return await axios
    .get("https://jsonplaceholder.typicode.com/posts")
    .then((resp) => resp.data);
}

async function getAxiosPostOne() {
  return await axios
    .get("https://jsonplaceholder.typicode.com/posts/1")
    .then((resp) => resp.data);
}

async function getAxiosCommentsOnPostOne() {
  return await axios
    .get("https://jsonplaceholder.typicode.com/posts/1/comments")
    .then((resp) => resp.data);
}

async function getAxiosCommentsOnPostIdOne() {
  return await axios
    .get("https://jsonplaceholder.typicode.com/comments?postId=1")
    .then((resp) => resp.data);
}

logData(getAxiosPosts);
logData(getAxiosPostOne);
logData(getAxiosCommentsOnPostOne);
logData(getAxiosCommentsOnPostIdOne);
